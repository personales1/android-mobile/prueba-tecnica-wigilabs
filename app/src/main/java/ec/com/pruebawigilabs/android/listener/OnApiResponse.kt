package ec.com.pruebawigilabs.android.listener

import ec.com.pruebawigilabs.android.retrofit.error.ServiceError

interface OnApiResponse<T> {
    fun onSuccess(response: T?) /*Exito 200-399*/
    fun onError(error: ServiceError) /*Error solicitud 400-499 excepto 401 ni 410*/
    fun onUnknownError() /*Cuando es un error y no se ha parseado el mensaje de error desde el server*/
    fun onFailure(throwable: Throwable) /*Error server >=500*/
    fun onWithoutInternet()
}