package ec.com.pruebawigilabs.android.retrofit.error

import ec.com.pruebawigilabs.android.retrofit.service.MyApiAdapter
import retrofit2.Response
import java.io.IOException

object ErrorUtils {
    fun parseError(response: Response<*>): ServiceError {
        val converter = MyApiAdapter.retrofit
            .responseBodyConverter<ServiceError>(
                ServiceError::class.java,
                arrayOfNulls(0)
            )
        val error: ServiceError
        error = try {
            converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            return ServiceError()
        }
        return error
    }
}