package ec.com.pruebawigilabs.android.retrofit.service

import android.content.Context
import ec.com.pruebawigilabs.android.helper.UtilidadesHelper
import ec.com.pruebawigilabs.android.listener.OnApiResponse
import ec.com.pruebawigilabs.android.retrofit.error.ErrorUtils
import ec.com.pruebawigilabs.android.retrofit.error.ServiceError
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object MyApiResources {
    fun <T> consumeApi(context: Context, o: Call<T>, callback: OnApiResponse<T>) {
        if (UtilidadesHelper.isInternetAvailable(context)) {
            o.enqueue(object : Callback<T> {
                override fun onResponse(
                    call: Call<T>,
                    response: Response<T>
                ) {
                    if (response.isSuccessful) {
                        callback.onSuccess(response.body())
                    } else {
                        val error: ServiceError? = ErrorUtils.parseError(response)
                        if (error == null) {
                            callback.onUnknownError()
                        } else {
                            callback.onError(error)
                        }
                    }
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    callback.onFailure(t)
                }
            })
        } else {
            callback.onWithoutInternet()
        }
    }
}