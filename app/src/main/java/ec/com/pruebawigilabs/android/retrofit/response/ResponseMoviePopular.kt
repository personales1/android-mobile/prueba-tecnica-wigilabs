package ec.com.pruebawigilabs.android.retrofit.response

import com.google.gson.annotations.SerializedName
import ec.com.pruebawigilabs.android.model.MoviePopular

class ResponseMoviePopular (
    @SerializedName("page") val page : Int,
    @SerializedName("total_pages") val totalPages : Int,
    @SerializedName("total_results") val totalResults : Int,
    @SerializedName("results") val results : ArrayList<MoviePopular>
)