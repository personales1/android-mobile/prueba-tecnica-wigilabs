package ec.com.pruebawigilabs.android.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import ec.com.pruebawigilabs.android.core.MyApp
import ec.com.pruebawigilabs.android.core.WigiLabsDB
import ec.com.pruebawigilabs.android.dao.MoviePopularDao
import ec.com.pruebawigilabs.android.databinding.ActivityMainBinding
import ec.com.pruebawigilabs.android.listener.OnApiResponse
import ec.com.pruebawigilabs.android.listener.OnMoviePopularListener
import ec.com.pruebawigilabs.android.model.MoviePopular
import ec.com.pruebawigilabs.android.retrofit.response.ResponseMoviePopular
import ec.com.pruebawigilabs.android.retrofit.error.ServiceError
import ec.com.pruebawigilabs.android.retrofit.service.MyApiAdapter
import ec.com.pruebawigilabs.android.retrofit.service.MyApiResources
import ec.com.pruebawigilabs.android.view.adapter.MoviePopularAdapter

class MainActivity : AppCompatActivity(), OnMoviePopularListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var moviePopularDao: MoviePopularDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        moviePopularDao = MyApp.myDB.moviePopularDao()

        binding.rcvMovies.layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
        getMoviePopular()
    }

    private fun getMoviePopular(){
        val apiService = MyApiAdapter.apiService()
        val objetoApi = apiService.MoviePopularList("09963e300150f9831c46a1828a82a984", "en-US")

        MyApiResources.consumeApi(this@MainActivity, objetoApi, object : OnApiResponse<ResponseMoviePopular> {
            override fun onSuccess(response: ResponseMoviePopular?) {
                binding.rcvMovies.adapter = MoviePopularAdapter(response!!.results, this@MainActivity)

                Thread {
                    moviePopularDao.insertAll(response.results)
                }.start()
            }

            override fun onError(error: ServiceError) {
            }

            override fun onUnknownError() {
            }

            override fun onFailure(throwable: Throwable) {
            }

            override fun onWithoutInternet() {
            }
        })
    }

    override fun selected(moviePopular: MoviePopular) {
        val intent = Intent(this@MainActivity, DetailMovieActivity::class.java)
        intent.putExtra("id_movie_popular", moviePopular.id)
        intent.putExtra("banner", moviePopular.posterPath)
        startActivity(intent)

    }
}