package ec.com.pruebawigilabs.android.core

import android.app.Application
import androidx.room.Room

class MyApp: Application() {
    companion object {
        lateinit var instance: MyApp
        lateinit var myDB: WigiLabsDB
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        myDB = Room.databaseBuilder(
            applicationContext,
            WigiLabsDB::class.java, "wigilabs"
        ).build()
    }
}