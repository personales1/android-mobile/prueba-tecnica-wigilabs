package ec.com.pruebawigilabs.android.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ec.com.pruebawigilabs.android.R
import ec.com.pruebawigilabs.android.core.MyApp
import ec.com.pruebawigilabs.android.listener.OnMoviePopularListener
import ec.com.pruebawigilabs.android.model.MoviePopular

class MoviePopularAdapter(private val list: ArrayList<MoviePopular>, private val listener: OnMoviePopularListener):
    RecyclerView.Adapter<MoviePopularAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_movie_popular, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemData = list[position]

        Glide.with(MyApp.instance.applicationContext)
            .load("https://www.themoviedb.org/t/p/w500_and_h282_face${itemData.backdropPath}")
            .error(R.drawable.im_error_photo)
            .placeholder(R.drawable.im_placeholder)
            .into(holder.imvMovie)

        holder.tvTitle.text = itemData.title
        holder.tvSmallDescription.text = itemData.overview

        holder.container.setOnClickListener {
            listener.selected(itemData)
        }
    }

    override fun getItemCount() = list.size

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var container: FrameLayout = view.findViewById(R.id.container)
        var imvMovie: ImageView = view.findViewById(R.id.imvMovie)
        var tvTitle: TextView = view.findViewById(R.id.tvTitle)
        var tvSmallDescription: TextView = view.findViewById(R.id.tvSmallDescription)
    }
}