package ec.com.pruebawigilabs.android.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import ec.com.pruebawigilabs.android.R
import ec.com.pruebawigilabs.android.core.MyApp
import ec.com.pruebawigilabs.android.dao.MoviePopularDao
import ec.com.pruebawigilabs.android.databinding.ActivityDetailMovieBinding
import ec.com.pruebawigilabs.android.databinding.ActivityMainBinding
import ec.com.pruebawigilabs.android.model.MoviePopular

class DetailMovieActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMovieBinding
    private lateinit var moviePopularDao: MoviePopularDao
    private lateinit var moviePopular: MoviePopular
    private lateinit var bannerMoviePopular: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)
        moviePopularDao = MyApp.myDB.moviePopularDao()

        val idMoviePopular = intent.getIntExtra("id_movie_popular", 0)
        bannerMoviePopular = intent.getStringExtra("banner")!!

        Glide.with(MyApp.instance.applicationContext)
            .load("https://www.themoviedb.org/t/p/w500_and_h282_face${bannerMoviePopular}")
            .error(R.drawable.im_error_photo)
            .placeholder(R.drawable.im_placeholder)
            .into(binding.imvBanner)

        Thread {
            moviePopular = moviePopularDao.getMoviePopularById(idMoviePopular)
            setData()
        }.start()

        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    fun setData() {
        binding.tvTitle.text = moviePopular.originalTitle
        binding.tvDescription.text = moviePopular.overview
        binding.tvDateRelease.text = moviePopular.releaseDate
        binding.tvVote.text = moviePopular.voteAverage
        binding.tvPopularity.text = moviePopular.popularity
    }
}