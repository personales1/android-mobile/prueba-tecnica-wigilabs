package ec.com.pruebawigilabs.android.listener

import ec.com.pruebawigilabs.android.model.MoviePopular

interface OnMoviePopularListener {
    fun selected(moviePopular: MoviePopular)
}