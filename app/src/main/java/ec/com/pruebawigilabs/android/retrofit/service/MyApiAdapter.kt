package ec.com.pruebawigilabs.android.retrofit.service

import android.os.Build
import ec.com.pruebawigilabs.android.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class MyApiAdapter {
    init{
        // Creamos un interceptor y le indicamos el log level a usar
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        // Asociamos el interceptor a las peticiones
        val clientBuilder = OkHttpClient.Builder()
        if(BuildConfig.DEBUG){
            clientBuilder.addInterceptor(loggingInterceptor)
        }

        clientBuilder.connectTimeout(60000, TimeUnit.SECONDS)
        clientBuilder.readTimeout(60000, TimeUnit.SECONDS)

        retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(clientBuilder.build())
            .build()

        myApiService = retrofit.create(MyApiService::class.java)
    }

    companion object {
        private var myApiAdapter : MyApiAdapter? = null
        lateinit var retrofit: Retrofit
        private lateinit var myApiService: MyApiService

        fun apiService(): MyApiService {
            if (myApiAdapter == null) {
                myApiAdapter = MyApiAdapter()
            }
            return myApiService
        }

        fun cleanService() {
            myApiAdapter = null
        }
    }
}