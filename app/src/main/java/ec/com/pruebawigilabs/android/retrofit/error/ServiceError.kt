package ec.com.pruebawigilabs.android.retrofit.error

import com.fasterxml.jackson.annotation.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("estado", "noticias")
class ServiceError {
    @JsonProperty("status_code")
    var statusCode: Int? = null

    @JsonProperty("status_message")
    var statusMessage: String = ""

    @JsonProperty("success")
    var success: Boolean = false

    /**
     * No args constructor for use in serialization
     */
    constructor() {}

    /**
     * @param message
     * @param status
     * @param mensaje_desa
     */
    constructor(status: Int, message: String, successerror: Boolean) {
        statusCode = status
        statusMessage = message
        success = successerror
    }
}