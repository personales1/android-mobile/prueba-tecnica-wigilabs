package ec.com.pruebawigilabs.android.core

import androidx.room.Database
import androidx.room.RoomDatabase
import ec.com.pruebawigilabs.android.dao.MoviePopularDao
import ec.com.pruebawigilabs.android.model.MoviePopular

@Database(
    entities = [MoviePopular::class],
    version = 1
)
abstract class WigiLabsDB : RoomDatabase() {
    abstract fun moviePopularDao(): MoviePopularDao
}