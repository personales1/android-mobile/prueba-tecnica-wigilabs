package ec.com.pruebawigilabs.android.retrofit.service

import ec.com.pruebawigilabs.android.retrofit.response.ResponseMoviePopular
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApiService {
    @GET("movie/popular")
    fun MoviePopularList(
        @Query("api_key") api_key: String,
        @Query("language") language: String
    ): Call<ResponseMoviePopular>
}