package ec.com.pruebawigilabs.android.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ec.com.pruebawigilabs.android.model.MoviePopular

@Dao
interface MoviePopularDao {
    @Query("SELECT * FROM MoviePopular WHERE id = :id")
    fun getMoviePopularById(id: Int): MoviePopular

    @Insert
    fun insertAll(moviePopular: ArrayList<MoviePopular>)
}